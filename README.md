# Main-subtree #

## Clone this repo
- `git clone git@bitbucket.org:game_kittipoom/main-subtree.git`

## Add subtree
- git remote add **<repo_name>** **<repo_url>**
	- eg. `git remote add subtree git@bitbucket.org:game_kittipoom/subtree.git`
- git subtree add --prefix=**<folder>** **<subtree_repo>** **<subtree_branch>**.
	- eg. `git subtree add --prefix=subtree subtree master`

## Push subtree
- git subtree push --prefix=**<folder>** **<subtree_repo>** **<subtree_branch>**
	- eg. `git subtree push --prefix=subtree subtree master`

## Pull subtree
- git subtree pull --prefix=**<folder>** **<subtree_repo>** **<subtree_branch>**
	- eg. `git subtree pull --prefix=subtree subtree master`


	